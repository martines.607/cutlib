package com.cutlet.lib.testing;

import com.cutlet.lib.model.Dimension;
import com.cutlet.lib.model.Panel;
import com.cutlet.lib.model.Project;
import com.cutlet.lib.model.StockSheet;

import java.util.ArrayList;
import java.util.List;

public class Test extends AbstractTestData {


    @Override
    public Project getData() {
        StockSheet sheet = this.makeSheet(24000, 600);
        boolean canRotate = false;
        List<Panel> inputs = new ArrayList();
        inputs.add(new Panel(sheet, new Dimension(120, 120), "p1", false, 1,canRotate,1));
        inputs.add(new Panel(sheet, new Dimension(300, 120), "p2", false, 1,canRotate,1));
        inputs.add(new Panel(sheet, new Dimension(260, 260), "p3", false, 1,canRotate,1));
        inputs.add(new Panel(sheet, new Dimension(240, 240), "p4", false, 1,canRotate,1));
        inputs.add(new Panel(sheet, new Dimension(205, 500), "p4", false, 1,canRotate,1));
        inputs.add(new Panel(sheet, new Dimension(230, 450), "p5", false, 1,canRotate,1));
        inputs.add(new Panel(sheet, new Dimension(200, 120), "p6", false, 1,canRotate,1));
        return makeProject(inputs, 1);
    }

    @Override
    public String getTitle() {
        return null;
    }
}
